package com.blogspot.rafishalahudin.quiz;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import java.util.ArrayList;

/**
 * Created by rafishalahudin on 05/01/18.
 */

public class QuizAdapter extends RecyclerView.Adapter<QuizAdapter.ViewHolder> {

    private ArrayList<QuizModel> list;

    public QuizAdapter(ArrayList<QuizModel> list) {
        this.list = list;
    }

    @Override
    public QuizAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(QuizAdapter.ViewHolder holder, int position) {
        final QuizModel name = list.get(position);
        holder.a.setText(name.getA());
        holder.b.setText(name.getB());
        holder.c.setText(name.getC());

        holder.a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    name.setJawabanUser(QuizModel.Pilihan.A);
            }
        });
        holder.b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    name.setJawabanUser(QuizModel.Pilihan.B);
            }
        });
        holder.c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    name.setJawabanUser(QuizModel.Pilihan.C);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public RadioButton a, b, c;

        public ViewHolder(View itemView) {
            super(itemView);

            a = itemView.findViewById(R.id.jawabana);
            b = itemView.findViewById(R.id.jawabanb);
            c = itemView.findViewById(R.id.jawabanc);
        }
    }
}
