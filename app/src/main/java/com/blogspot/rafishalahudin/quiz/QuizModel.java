package com.blogspot.rafishalahudin.quiz;

/**
 * Created by rafishalahudin on 05/01/18.
 */

public class QuizModel {
    private String A;
    private String B;
    private String C;
    private Pilihan JawabanBenar;
    private Pilihan JawabanUser;

    public enum Pilihan{
        A,B,C
    }

    public QuizModel(String a, String b, String c, Pilihan jawabanBenar) {
        A = a;
        B = b;
        C = c;
        JawabanBenar = jawabanBenar;
    }

    public String getA() {
        return A;
    }

    public void setA(String a) {
        A = a;
    }

    public String getB() {
        return B;
    }

    public void setB(String b) {
        B = b;
    }

    public String getC() {
        return C;
    }

    public void setC(String c) {
        C = c;
    }

    public Pilihan getJawabanBenar() {
        return JawabanBenar;
    }

    public void setJawabanBenar(Pilihan jawabanBenar) {
        JawabanBenar = jawabanBenar;
    }

    public Pilihan getJawabanUser() {
        return JawabanUser;
    }

    public void setJawabanUser(Pilihan jawabanUser) {
        JawabanUser = jawabanUser;
    }
}
