package com.blogspot.rafishalahudin.quiz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvView;
    private QuizAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<QuizModel> dataSet= new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvView = findViewById(R.id.rv_main);
        rvView.setHasFixedSize(true);

        /**
         * Kita menggunakan LinearLayoutManager untuk list standar
         * yang hanya berisi daftar item
         * disusun dari atas ke bawah
         */
        layoutManager = new LinearLayoutManager(this);
        rvView.setLayoutManager(layoutManager);

        adapter = new QuizAdapter(dataSet);
        rvView.setAdapter(adapter);

        initDataset();

        Button b = findViewById(R.id.submit);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int nilai = 0;



                for(int i = 0;i<dataSet.size();i++){
                    if(dataSet.get(i).getJawabanUser() != null) {
                        if (dataSet.get(i).getJawabanUser().equals(dataSet.get(i).getJawabanBenar())) {
                            nilai++;
                            Log.wtf("onClick: ", "benar");
                        }
                    }
                }

                Toast.makeText(MainActivity.this, nilai+"", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void initDataset(){

        /**
         * Tambahkan item ke dataset
         * dalam prakteknya bisa bermacam2
         * tidak hanya String seperti di kasus ini
         */

        QuizModel model = new QuizModel("INi a","INib","Ini c", QuizModel.Pilihan.A);
        dataSet.add(model);
        QuizModel model1 = new QuizModel("askdjaklsd","INib","Ini c", QuizModel.Pilihan.A);
        dataSet.add(model1);
        QuizModel model2 = new QuizModel("INi a","INib","Ini c", QuizModel.Pilihan.A);
        dataSet.add(model2);
        adapter.notifyDataSetChanged();
    }
}
